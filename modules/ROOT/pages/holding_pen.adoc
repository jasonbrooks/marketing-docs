= Holding Pen

This page holds content removed for later review.
It was originally on a wiki page, but may not be relevant anymore.

=== Contributors

The [[Contributors to the marketing group | contributors page]] lists '''active''' and inactive Fedora Marketing contributors. Note that you must be recently active to maintain FAS marketing membership. 

"Recently" means within the last release cycle. So, if you made a contribution to the previous or current Fedora release - you're considered active. If your contributions are older than that, then we would consider you an inactive member. That's OK, life happens! But for purposes of deciding FAS membership (and granting rights to vote in certain Fedora elections), we want to ensure that folks remain active. 

== Release deliverables ==

The Marketing team is responsible for producing marketing release deliverables for each Fedora release. Our team's deliverables are focused on the Desktop Spin; we also provide resources to help other groups within Fedora make similar marketing deliverables for their projects.

Our work in progress on Desktop Spin deliverables for the current release, as well as the marketing release deliverables for all current actively supported Fedora releases (the prior 2 releases), can be found in the table below.

For more information on each deliverable type, including instructions on how to make them (Standard Operating Procedures, or SOPs) and non-Desktop-Spin variants produced by other teams for each release, click on the name of the deliverable in the table below.

{| class="wikitable"
! Release deliverable type !! SOP (how to make) !! Last supported release !! Current release !! Release in development
|-
| [[Talking points]] || [[Talking points SOP]] || [[Fedora_{{FedoraVersionNumber|previous}}_talking_points | F{{FedoraVersionNumber|previous}} talking points]] || [[Fedora_{{FedoraVersionNumber|current}}_talking_points | F{{FedoraVersionNumber|current}} talking points]] || [[Fedora_{{FedoraVersionNumber|next}}_talking_points | F{{FedoraVersionNumber|next}} talking points]]
|-
| [[Feature profiles]] || [[Feature profiles SOP]] || [[F{{FedoraVersionNumber|previous}} feature profiles]] || [[F{{FedoraVersionNumber|current}} feature profiles]] || [[F{{FedoraVersionNumber|next}} feature profiles]]
|-
| [[Screenshots library]] || [[Screenshots library SOP]] || (new in F{{FedoraVersionNumber|current}}) ||[[F{{FedoraVersionNumber|current}}_screenshots_library]] || [[F{{FedoraVersionNumber|next}}_screenshots_library]]
|-
| [[Alpha announcement]] || [[Alpha announcement SOP]] || [[F{{FedoraVersionNumber|previous}} Alpha release announcement]] || [[F{{FedoraVersionNumber|current}} Alpha release announcement]] || [[F{{FedoraVersionNumber|next}} Alpha release announcement]]
|-
| [[Beta announcement]] || [[Beta announcement SOP]] || [[F{{FedoraVersionNumber|previous}} Beta release announcement]] || [[F{{FedoraVersionNumber|current}} Beta release announcement]] ||  [[F{{FedoraVersionNumber|next}} Beta release announcement]]
|-
| [[One page release notes]] || [[One page release notes SOP]] || (new in F{{FedoraVersionNumber|current}}) || [[F{{FedoraVersionNumber|current}} one page release notes]] || [[Fedora_21_release_notes|F{{FedoraVersionNumber|next}} one page release notes]]
|-
| [[Ambassadors briefing]] || [[Ambassadors briefing SOP]] || (new in F{{FedoraVersionNumber|next}}) || (new in F{{FedoraVersionNumber|next}}) || [[F{{FedoraVersionNumber|next}} Ambassadors briefing]] 
|-
| [[Final announcement]] || [[Final announcement SOP]] || [[F{{FedoraVersionNumber|previous}} release announcement]] || [[F{{FedoraVersionNumber|current}} release announcement]] || [[F{{FedoraVersionNumber|next}} release announcement]]
|-
| [[Press kit]] || [[Press kit SOP]] || [[Fedora {{FedoraVersionNumber|previous}} press kit]] || [[Fedora {{FedoraVersionNumber|current}} press kit]] || [[Fedora {{FedoraVersionNumber|next}} press kit]]
|-
| [[Marketing retrospective]] || [[Marketing retrospective SOP]] || (new in F{{FedoraVersionNumber|current}}) || [[F{{FedoraVersionNumber|current}} marketing retrospective]] || [[F{{FedoraVersionNumber|next}} marketing retrospective]]
|}

== Release activities ==

* Briefing Ambassadors
* Coordinating PR
* Monitoring PR/news from the release 
* [[Fedora_news_distribution_network_%28NDN%29|Fedora news distribution network (FNDN)]]
** sending out beta release announcements
** send out Release Information to the press
